/*
 * misc.c
 *
 *  Created on: 2017/03/07
 *      Author: tattaka
 */

#include "misc.h"
#include "myPWM.h"
#include "machine_state.h"
#include "stdbool.h"
#include "parameter.h"

int mode;
void HAL_delay_us(uint32_t Delay){
	/*
	HAL_TIM_Base_Start_IT(&htim11);
	while(Delay > ticknow){
	}
	HAL_TIM_Base_Stop_IT(&htim11);
	ticknow = 0;*/
}

void music(uint32_t pulse, uint32_t prescaler, uint32_t delay, int state){
	if(state == 1){
	MUSIC_TIM.Init.Prescaler = prescaler;
	HAL_TIM_Base_Init(&MUSIC_TIM);
	myPWM_ConfigChannel(&MUSIC_TIM, pulse, MUSIC_CHAN);
	HAL_TIM_PWM_Start(&MUSIC_TIM, MUSIC_CHAN); //starts PWM on CH1N pin

	HAL_Delay(delay);
	}
	else if(state == 0){
	HAL_TIM_PWM_Stop(&MUSIC_TIM, MUSIC_CHAN);
	}
}

void mode_select(){
	bool flag = true;
	int mode_temp = 0;
	int mode_buff = 0;
	int musiccount[14] = {1532, 1362, 1213, 1145, 1020, 909, 809, 764, 681, 607, 572, 510, 454, 404};

	for(int i = 0; i < 3; i++){
		encoder_R[i] = 0;
		encoder_L[i] = 0;
	}
	HAL_TIM_Encoder_Start(&htim2,TIM_CHANNEL_1);
	HAL_TIM_Encoder_Start(&htim2,TIM_CHANNEL_2);
	HAL_TIM_Encoder_Start(&htim3,TIM_CHANNEL_1);
	HAL_TIM_Encoder_Start(&htim3,TIM_CHANNEL_2);
	while(flag){
		mode_temp = encoder_R[0] / ONEROTATE_TIRE;
		if(mode_temp > 6){
			mode_temp = 6;
			encoder_R[0] = ONEROTATE_TIRE * 6;
		}
		if(mode_temp < 0){
			mode_temp = 0;
			encoder_R[0] = 0;
		}
		if((mode_temp-mode_buff)!=0){
			music(5, musiccount[6-(mode_temp-mode_buff)], 100, 1);
			music(0, 0, 0, 0);
		}
		DEBUG_LED1_RESET();
		DEBUG_LED2_RESET();
		DEBUG_LED3_RESET();
		DEBUG_LED4_RESET();
		DEBUG_LED5_RESET();
		DEBUG_LED6_RESET();
		if(mode_temp == 1){
			DEBUG_LED1_SET();
		}
		else if(mode_temp == 2){
			DEBUG_LED1_SET();
			DEBUG_LED2_SET();
		}
		else if(mode_temp == 3){
			DEBUG_LED1_SET();
			DEBUG_LED2_SET();
			DEBUG_LED3_SET();
		}
		else if(mode_temp == 4){
			DEBUG_LED1_SET();
			DEBUG_LED2_SET();
			DEBUG_LED3_SET();
			DEBUG_LED4_SET();
		}
		else if(mode_temp == 5){
			DEBUG_LED1_SET();
			DEBUG_LED2_SET();
			DEBUG_LED3_SET();
			DEBUG_LED4_SET();
			DEBUG_LED5_SET();
		}
		else if(mode_temp == 6){
			DEBUG_LED1_SET();
			DEBUG_LED2_SET();
			DEBUG_LED3_SET();
			DEBUG_LED4_SET();
			DEBUG_LED5_SET();
			DEBUG_LED6_SET();
		}

		/*
		if(sensor_digital_read()==1){
			if((mode_temp>=1)&&(mode_temp<2)){
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_2);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_2);
				mode = mode_temp;
				for(int i = 0; i < 3; i++){
					encoder_R[i] = 0;
					encoder_L[i] = 0;
				}
				flag = false;
			}
			if((mode_temp>=2)&&(mode_temp<3)){
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_2);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_2);
				mode = mode_temp;
				for(int i = 0; i < 3; i++){
					encoder_R[i] = 0;
					encoder_L[i] = 0;
				}
				flag = false;
			}
			if((mode_temp>=3)&&(mode_temp<4)){
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_2);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_2);
				mode = mode_temp;
				for(int i = 0; i < 3; i++){
					encoder_R[i] = 0;
					encoder_L[i] = 0;
				}
				flag = false;
			}
			if((mode_temp>=4)&&(mode_temp<5)){
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_2);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_2);
				mode = mode_temp;
				for(int i = 0; i < 3; i++){
					encoder_R[i] = 0;
					encoder_L[i] = 0;
				}
				flag = false;
			}
			if((mode_temp>=5)&&(mode_temp<6)){
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_2);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_2);
				mode = mode_temp;
				for(int i = 0; i < 3; i++){
					encoder_R[i] = 0;
					encoder_L[i] = 0;
				}
				flag = false;
			}
			if((mode_temp>=6)&&(mode_temp<7)){
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim2,TIM_CHANNEL_2);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_1);
				HAL_TIM_Encoder_Stop(&htim3,TIM_CHANNEL_2);
				mode = mode_temp;
				for(int i = 0; i < 3; i++){
					encoder_R[i] = 0;
					encoder_L[i] = 0;
				}
				flag = false;
			}
		}*/
		mode_buff = mode_temp;
	}
}
