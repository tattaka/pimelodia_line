/*
 * misc.h
 *
 *  Created on: 2017/03/07
 *      Author: tattaka
 */

#ifndef USERINC_MISC_H_
#define USERINC_MISC_H_

#include "stm32f4xx_hal.h"
#include "robot.h"
#include "tim.h"

void HAL_delay_us(uint32_t);
void music(uint32_t, uint32_t, uint32_t, int);
void mode_select();

#endif /* USERINC_MISC_H_ */
